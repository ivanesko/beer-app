import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
Vue.use(Vuex, axios);
export default new Vuex.Store({
  state: {
    beers: [],
    savedBeers: []
  },
  mutations: {
    SET_BEERS(state, beers) {
      state.beers = beers;
    },
    ADD_BEER(state, payload) {
      state.savedBeers.push(payload);
    }
  },
  actions: {
    loadBeers({ commit }) {
      axios
        .get("https://api.punkapi.com/v2/beers?page=2&per_page=10")
        .then(data => {
          let beers = data.data;
          commit("SET_BEERS", beers);
        })
        .catch(error => {
          const errorcontainer = error;
          errorcontainer();
        });
    },
    addBeer(context, beer) {
      context.commit("ADD_BEER", beer);
    }
  },
  getters: {
    getBeer(state) {
      return state.savedBeers;
    }
  }
});
